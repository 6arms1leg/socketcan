.. socketcan documentation master file, created by
   sphinx-quickstart on Sat Jun  5 19:55:40 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to socketcan's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/api
   usage/tutorial


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
