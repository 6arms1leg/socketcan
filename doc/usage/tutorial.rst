Tutorial
========

Sending a Can Frame
-------------------

Simplest way to send a single CanFrame is to use CanRawSocket.
It has no logic behind it, it just sends and receives CanFrames on a given interface.

.. code-block:: python

    from socketcan import CanRawSocket, CanFrame

    interface = "vcan0"
    s = CanRawSocket(interface=interface)

    can_id = 0x12345678
    data = bytes(range(0,0x88,0x11))
    frame1 = CanFrame(can_id=can_id, data=data)

    s.send(frame1)


Now you should see an output in candump.

.. code-block:: shell-session

    vcan0  12345678   [8]  00 11 22 33 44 55 66 77

